import React from 'react';
import '../../../node_modules/font-awesome/css/font-awesome.min.css';

export default function Icons(props){
  
        const {classIcon} = props;
        return(
            <i className={classIcon}></i>
        )
    }
