import React from 'react';
import '../../../node_modules/font-awesome/css/font-awesome.min.css';
import Fab from '@material-ui/core/Fab';

export default function IconButton(props){
  
        const {classIcon,className,color} = props;
        return(
            <Fab color={color} aria-label="Add" className={className}>
            <i className={classIcon}></i>
          </Fab>
        )
    }
