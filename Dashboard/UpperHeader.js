import React, { Component } from 'react';
import 'font-awesome/css/font-awesome.min.css';
import './UpperHeader.css';
import Typography from '@material-ui/core/Typography';

export default class UpperHeader extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Header: [
                { title: ' Student', icon: 'graduation-cap', Count: 1004, color: '#FF8A65' },
                { title: ' Teacher', icon: 'male', Count: 5, color: '#4DB6AC' },
                { title: ' Section', icon: 'line-chart', Count: 3, color: '#F06292' },
                { title: ' Due Students', icon: 'bank', Count: 1, color: '#9575CD' }
            ],
        }
    }

    render() {
   
        let { Header } = this.state;
        return (

            <Typography>
                <div className="container"> 
                <div className="row">
                {Header.map(items=>{
                    return(
                        <div className="col-sm-6 col-md-3 col-lg-3 col-xl-3" style={{margin:'30px 0px 0px 0px' }} >
                            <div className="mainHeaderDiv" style={{width: '233px',padding:"10px",margin: '0px 15px' ,borderRadius:"5px",height: '90px',backgroundColor:`${items.color}` }}>
                        <div className="textDiv" style={{width:'70%', float:'left',height: '100%'}}>
                            <p><h3 > {items.Count}</h3></p>
                            <p>{items.title}</p>
                        </div>
                        <div className="iconsDiv" style={{width:'30%',marginTop:"8px" , float:'right',height: '100%'  }}>
                            <i className={`fa fa-${items.icon}`} style={{fontSize: '52px'}}> </i>
                        </div>

                        

                        </div>
                        </div>
                    )
                })}
                    
                </div>
                </div>

            </Typography>
        )
    }
}